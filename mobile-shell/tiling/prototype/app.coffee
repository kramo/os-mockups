w = 1280
h = 768

appdrawerHeight = 530
windowHeight = 728
windowGap = 50
panelHeight = 40

currentOverviewState = "SESSION"    # SESSION / OVERVIEW
currentShellState = "SESSION"       # TIME / SESSION / STATUS

currentWorkspace = 1                # 0 / 1 / 2


animOptions =
  time: 0.2
  curve: Spring(damping: 0.95)
  # curve: Bezier.easeOut

slower =
  time: 0.34
  curve: Spring(damping: 0.86)

slow =
  time: 0.28
  curve: Spring(damping: 0.9)

medium =
  time: 0.2
  curve: Spring(damping: 0.97)

fast =
  time: 0.12
  curve: Spring(damping: 0.97)

faster =
  time: 0.04
  curve: Spring(damping: 0.99)

# CONTAINERS

bg = new Layer
  width: w
  height: h
  image: "assets/bg+panels.png"
  shadow1:
    y: 8
    blur: 20

container = new Layer
  parent: bg
  width: w
  height: h
  x: 0
  y: Align.center
  scale: 1
  image: "assets/wallpaper.png"
  animationOptions: animOptions
  # brightness: 100

container.states.time =
  scale: 0.9
  x: 310
  # brightness: 70

container.states.system =
  scale: 0.9
  x: -330
  # brightness: 70


# WINDOWS

windowCarousel = new PageComponent
  parent: container
  width: w
  height: h
  y: Align.top
  scrollVertical: false
  scrollHorizontal: false
  animationOptions: animOptions
  ignoreEvents: true

windows = new Layer
  parent: windowCarousel.content
  width: w * 3 + windowGap * 2      # 3940
  height: windowHeight
  scale: 1
  x: -w - windowGap
  y: Align.top
  animationOptions: slow
  image: "assets/windows.png"

windows.states.overview =
  scale: 0.28
  x: -w - windowGap
  y: -250

windows.states.window2 =
  scale: 1
  x: -w - windowGap - (w + windowGap)
  y: Align.top

windows.states.window2overview =
  scale: 0.28
  x: -w - windowGap - (w + windowGap) * 0.28
  y: -250


# APP DRAWER

appdrawerBg = new Layer
  parent: container
  width: w
  height: 0.01
  y: h
  image: "assets/appdrawer-bg.png"
  animationOptions: fast

appdrawerBg.states.overview =
  height: appdrawerHeight
  y: h - appdrawerHeight

appdrawerCarousel = new PageComponent
  parent: container
  width: w
  height: appdrawerHeight
  y: Align.bottom
  originY: 0
  scrollHorizontal: false
  animationOptions: medium
  ignoreEvents: true

panel = new Layer
  parent: appdrawerCarousel.content
  width: w
  height: appdrawerHeight
  y: 0
  image: "asssets/appdrawer-panel-empty.png"
  animationOptions: fast

time = new Layer
  parent: panel
  width: 100
  height: panelHeight
  x: 0
  y: Align.bottom
  image: "assets/appdrawer-time.png"
  animationOptions: fast

time.states.active =
  image: "assets/appdrawer-time-active.png"

arrow = new Layer
  parent: panel
  width: 200
  height: panelHeight
  x: Align.center
  y: Align.bottom
  image: "assets/appdrawer-arrow.png"
  animationOptions: faster

arrow.states.overview =
  image: "assets/appdrawer-arrow-down.png"

systemStatus = new Layer
  parent: panel
  width: 100
  height: panelHeight
  x: w - 100
  y: Align.bottom
  image: "assets/appdrawer-system.png"
  animationOptions: fast

systemStatus.states.active =
  image: "assets/appdrawer-system-active.png"

appDrawer = new Layer
  # parent: appdrawerCarousel.content
  width: w
  height: appdrawerHeight - panelHeight
  image: "assets/appdrawer-rest.png"

appdrawerCarousel.addPage(appDrawer, "bottom")


darken = new Layer
  parent: container
  width: w
  height: h
  x: Align.center,
  y: Align.center
  backgroundColor: "#241f31"
  scale: 1/60
  ignoreEvents: true
  opacity: 0
  animationOptions: fast

darken.states.visible =
  scale: 1
  opacity: 0.3
  ignoreEvents: false

# ACTIONS

time.onTap ->
  if (currentShellState == "SESSION")
    currentShellState = "TIME"
    container.animate("time")
    time.animate("active")
    darken.animate("visible")

systemStatus.onTap ->
  if (currentShellState == "SESSION")
    currentShellState = "SYSTEM"
    container.animate("system")
    systemStatus.animate("active")
    darken.animate("visible")

arrow.onTap ->
  if (currentOverviewState == "SESSION")
    currentOverviewState = "OVERVIEW"
    appdrawerCarousel.snapToPage( appDrawer
                                  true
                                  medium )
    arrow.stateSwitch("overview")
    appdrawerBg.animate("overview")
    if (currentWorkspace == 1)
      windows.animate("overview")
    else
      windows.animate("window2overview")
  else
    currentOverviewState = "SESSION"
    appdrawerCarousel.snapToPage( panel
                                  true
                                  medium )
    arrow.stateSwitch("default")
    appdrawerBg.animate("default")
    if (currentWorkspace == 1)
      windows.animate("default")
    else
      windows.animate("window2")

windows.onTap ->
  if (currentOverviewState == "OVERVIEW")
    if (currentWorkspace == 1)
      currentWorkspace = 2
      windows.animate("window2overview")
    else
      currentWorkspace = 1
      windows.animate("overview")

darken.onTap ->
  currentShellState = "SESSION"
  container.animate("default")
  darken.animate("default")
  time.animate("default")
  systemStatus.animate("default")
